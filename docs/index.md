n3t Multi Captcha
=================

[![Version](https://img.shields.io/badge/Latest%20version-5.0.0-informational)][N3TMULTICAPTCHA]
[![Release date](https://img.shields.io/badge/Release%20date-2024--06--13-informational)][N3TMULTICAPTCHA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.4-green)][PHP]

n3t Multi Captcha allows to randomly switch between selected Captcha plugins.
It also add additional security level by checking users IP address against black lists.

Installation
------------

Install n3t Multi Captcha as any other [Joomla! CMS][JOOMLA] plugin. After installation do not
forget to publish it, check it's options and set it as default Captcha plugin in Joomla Global configuration.


[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[N3TMULTICAPTCHA]: https://n3t.bitbucket.io/extension/n3t-multi-captcha
