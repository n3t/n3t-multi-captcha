Usage
=====

n3t Multi Captcha is fully integrated into Joomlu using its powerful plugin system.
This means, it can be used with that any extension in Joomla using Captcha plugin 
system. If it doesn't work with your extension, means that the extension is not using 
Joompla Captcha plugin system, and solving Captcha by itself. Ask developer of that 
extension if it si possible to switch to standard Joomla methods.

How does it work?
=================

When enabled and set as default Captcha plugin n3t Captcha can do following:

- display other Captcha (like ReCaptcha), and if you chjoose multiple Captcha plugins, it will randomly rotate between those.
- log any blocked attempts, so if your users are not able to submit form, you can find out the reason and help them to solve it.
- check user IP against blacklists: 
    - StopForumSpam.com
    - SpamHaus.org
    - Sorbs.net
    - SpamCop.net
    - UceProtect.net
    - DroneBl.org
    - Botscout.com
    - Project Honey Pot
    - manual blacklist of IPs / IP ranges
- check users email address (when they are logged in) against blacklists:
    - StopForumSpam.com
    - Botscout.com
- check submitted content with following checks
    - disallow links (containing `http://` or `www.`) 
    - disallow any HTML tags
    - disallow PHP `<?` tags
    - disallow `<script>` tags
    - checks characters in content and whitelist / blacklist character sets (like Latin, Cyrillic etc.)
    - check content against Akismet API
    - check content against words and regular expressions blacklist
- try to detect language of content and whitelist / blacklist selected languages
- detect GeoIP of user, based on internal PHP functions (if available), MaxMind.com
  API (free GeoLite2 or paid GeoIP2) and/or StopForumSpam.com API and whitelist / blacklist continents 
  and/or countries
- add additional nternal checks:
    - sessions check
    - cookie check
    - minimal submit time for a form
    - hidden honeypot field check
    - javascript support check
- whitelist IPs / range of IPs so that no Captcha is displayed at all