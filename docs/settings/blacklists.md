Blacklists
==========

StopForumSpam.com
-----------------

When enabled, visitor IP address is checked against [StopForumSpam.com][STOPFORUMSPAM] blacklist.
Additionally, when user is logged in, his email address is also checked. If any of these is listed in 
StopForumSpam.com database, the submission is blocked.

### Block Tor Exit nodes

When this is enabled, and users IP address is detected as [TOR exit node][TORWIKI],
submission is blocked.

### Maximal confidence tolerance (‰)

Set maximal tolerated confidence of StopForumSpam.com report. If IP address, or email appears 
in StopForumSpam.com database, it has calculated so called confidence score.
This confidence score is a reasonably good indicator that the field under test would result in unwanted activity.

In this parameter you can set maximal tolerated confidence, which will still pass this test.
Higher value you set, lower chance to have false positive results, but higher chance not to stop real spam.

!!! note 
    Value is set in __promiles__ (‰). Means value of 1 means 0.1 % (percent). 


SpamHaus.org
------------

When enabled, visitor IP address is checked against [SpamHaus.org][SPAMHAUS] DNS blacklist.
If listed, submission is blocked.

Sorbs.net
---------

When enabled, visitor IP address is checked against [Sorbs.net][SORBS] DNS blacklist.
If listed, submission is blocked.

problems.dnsbl.sorbs.net and l2.spews.dnsbl.sorbs.net zones are being checked.

SpamCop.net
-----------

When enabled, visitor IP address is checked against [SpamCop.net][SPAMCOP] DNS blacklist.
If listed, submission is blocked.


UceProtect.net
--------------

When enabled, visitor IP address is checked against [UceProtect.net][UCEPROTECT] DNS blacklist.
If listed, submission is blocked.

All three lists (dnsbl-1.uceprotect.net, dnsbl-2.uceprotect.net and dnsbl-3.uceprotect.net) are being checked.

DroneBl.org
-----------

When enabled, visitor IP address is checked against [DroneBl.org][DRONEBL] DNS blacklist.
If listed, submission is blocked.

BotScout.com
------------

When enabled, visitor IP address is checked against [BotScout.com][BOTSCOUT] blacklist.
Additionally, when user is logged in, his email address is also checked. If any of these is listed in
BotScout.com database, the submission is blocked.

### BotScout.com API key

To run more than about ~400 bot checks against BotScout database per day you need to [get an API Key][BOTSCOUT-APIKEY].

### BotScout.com max occurrences

Set maximal tolerated occurrences of BotScout report. If IP address, or email appears
in BotScout database, it reports also how many times it was reported. 

Here you can set maximal tolerated reports number.
Higher value you set, lower chance to have false positive results, but higher chance not to stop real spam.

Project Honey Pot
-----------------

When enabled, visitor IP address is checked against [Project HoneyPot][PROJECTHONEYPOT] DNS blacklist.
If listed, submission is blocked.

### Project Honey Pot API key

To run queries against Project Honey Pot Api, you need an API key.
Get one at [Project Honey Pot][PROJECTHONEYPOT] website.

IP Blacklist
------------

Here you can specify list of IP addresses always blocked. Enter either separate IP address, 
or range like 192.168.0.1-192.168.0.50, or in CIDR format like 192.168.0.0/24.

[STOPFORUMSPAM]: https://www.stopforumspam.com/
[SPAMHAUS]: https://www.spamhaus.org/
[SORBS]: https://www.sorbs.net/
[SPAMCOP]: https://www.spamcop.net/
[UCEPROTECT]: https://www.uceprotect.net/en
[DRONEBL]: https://www.dronebl.org/
[BOTSCOUT]: https://www.botscout.com/
[BOTSCOUT-APIKEY]: https://botscout.com/getkey.htm
[PROJECTHONEYPOT]: https://www.projecthoneypot.org/
[TORWIKI]: https://en.wikipedia.org/wiki/Tor_(network)