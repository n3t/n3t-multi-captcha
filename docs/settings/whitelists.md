Whitelists
==========

Here you can specify Whilists. Whitelisted IP addresses will not be checked
by Captcha at all. 

Whitelist localhost
-------------------

When enabled, localhost (127.0.0.1 or ::1) will be whitelisted automatically.

Whitelist private networks
--------------------------

When enabled, private networks (10.0.0.0 – 10.255.255.255, 172.16.0.0 – 172.31.255.255, 192.168.0.0 – 192.168.255.255, fd00::/8) 
will be whitelisted automatically.

IP Whitelist
------------

Here you can manually specify list of whitelisted IP addresses. Enter either separate IP address,
or range like 192.168.0.1-192.168.0.50, or in CIDR format like 192.168.0.0/24.
