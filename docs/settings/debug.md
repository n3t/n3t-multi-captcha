Debug settings
==============

!!! warning
    Use these settings just for debugging and testing purposes. Do not leave it 
    on life site, or all your users will be handled by these values.

Debug IP
--------

Fill here IP address that will be used for debugging purposes instead of your real IP address. 

Debug user name
---------------

Fill here user name that will be used for debugging purposes instead of real user name.

Debug user email
----------------

Fill here user email that will be used for debugging purposes instead of real user email. 