Language settings
================

!!! warning
    Be careful with language detection, mainly if using as whitelist. Language detection is not very reliable these days yet, 
    so you can easily block out all your users.

Use internal language detection
-------------------------------

### Language detection minimal score

### Language detection minimal ngrams

Use DetectLanguage.com API
--------------------------

### DetectLanguage.com API key

### DetectLanguage.com minimal confidence

Minimal length of submission text
---------------------------------

Language list mode
------------------

Language list
-------------