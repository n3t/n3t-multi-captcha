Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Unreleased
----------

- no unreleased changes

5.0.x
-----

### [5.0.0] - 2024-06-13

#### Changed
- Joomla 5 compatibility
- Improved debug for displaying captcha
#### Fixed
- use of inline scripts and styles for inline cached forms

4.0.x
-----

### [4.0.6] - 2023-01-30

#### Changed
- additional check for selected captcha plugin for invalid cached forms (Forms should be never cached, as the Captcha plugin is not properly initialised)
- additional checks for PHP built-in GeoIP function result
#### Added
- option to choose GeoIp database path for built-in PHP GeoIp detection

### [4.0.5] - 2023-01-13

#### Changed
- SpamHaus, Sorbs and SpamCop checks are not default now, after installation.
  It provided some false positives, blocking real users. Be careful, when using it.
- SpamHaus check ignores PBL records and error response now. This was leading to false positives sometimes.
- Debug mode is now allowed only for listed IPs - reduces chance to leave Debug mode enabled by accident.
- StopForumSpam check uses now only hash of email, to be more GDPR-compliant.

### [4.0.4] - 2022-09-06

#### Fixed
- Correct min_time and cookie check parameters

### [4.0.3] - 2022-07-12

#### Fixed
- Virtuemart compatibility (Virtuemart does not use Joomla standards for Captcha)

### [4.0.2] - 2022-07-09

#### Added
- maximal allowed links parameter, if links are blocked

### [4.0.1] - 2022-06-13

#### Fixed
- error 'Interface 'Tracy\IBarPanel' not found' when n3tDebug installed but not enabled for current IP

### [4.0.0] - 2022-06-07

- Initial public release
