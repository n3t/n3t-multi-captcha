Credits
=======

Thanks to these projects and contributors:

- [Joomla! CMS][Joomla] withou whic, obvously, this extension will not exist
- [Bongovo!][BONGOVO] for Czech and Slovak translation
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development
- [Patrick Schur][PATRICKSCHUR] for his PHP library for language detection  
- [StopForumSpam.com][STOPFORUMSPAM] for their SPAM database and API
- [SpamHaus.org][SPAMHAUS] for their SPAM DNS Blacklist
- [Sorbs.net][SORBS] for their SPAM DNS Blacklist
- [SpamCop.net][SPAMCOP] for their SPAM DNS Blacklist
- [UceProtect.net][UCEPROTECT] for their SPAM DNS Blacklist
- [DroneBl.org][DRONEBL] for their SPAM DNS Blacklist
- [BotScout.com][BOTSCOUT] for their SPAM database and API
- [Project Honey Pot][PROJECTHONEYPOT] for their SPAM DNS Blacklist
- [Akismet.com][AKISMET] for their SPAM database and API
- [DetectLanguage.com][DETECTLANGUAGE] for their language detection API
- [MaxMind.com][MAXMIND] for their GeoIP API

[JOOMLA]: https://www.joomla.org
[BONGOVO]: https://www.bongovo.cz/
[JOOMLAPORTAL]: https://www.joomlaportal.cz/
[PATRICKSCHUR]: https://github.com/patrickschur/language-detection
[STOPFORUMSPAM]: https://www.stopforumspam.com/
[SPAMHAUS]: https://www.spamhaus.org/
[SORBS]: https://www.sorbs.net/
[SPAMCOP]: https://www.spamcop.net/
[UCEPROTECT]: https://www.uceprotect.net/en
[DRONEBL]: https://www.dronebl.org/
[BOTSCOUT]: https://www.botscout.com/
[PROJECTHONEYPOT]: https://www.projecthoneypot.org/
[AKISMET]: https://www.akismet.com/
[DETECTLANGUAGE]: https://www.detectlanguage.com/
[MAXMIND]: https://www.maxmind.com/