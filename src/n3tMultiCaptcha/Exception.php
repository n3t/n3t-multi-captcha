<?php
/**
 * @package n3t MultiCaptcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2022 - 2023 Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace Joomla\Plugin\Captcha\n3tMultiCaptcha;

\defined('_JEXEC') or die;

class Exception extends \Exception {
}
