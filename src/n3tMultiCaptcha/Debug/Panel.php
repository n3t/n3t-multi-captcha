<?php
/**
 * @package n3t MultiCaptcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2022 - 2023 Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace Joomla\Plugin\Captcha\n3tMultiCaptcha\Debug;

\defined('_JEXEC') or die;

use n3tDebug\Helper;

class Panel extends \n3tDebug\Panel
{

	/**
	 * @var \plgCaptchaN3tMultiCaptcha
	 * @since 4.0.0
	 */
	private $plugin = null;

	public function __construct(\plgCaptchaN3tMultiCaptcha $plugin)
	{
		$this->plugin = $plugin;
		parent::__construct($plugin->params);
	}

	public function setPlugin(\plgCaptchaN3tMultiCaptcha $plugin): void
	{
		$this->plugin = $plugin;
	}

	public function collectData(): void
  {
	  $this->data = $this->plugin->collectDebugData();
  }

  protected function getIcon(): string
  {
    $html = '';

    if ($this->data) {
      $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="32" height="32" style="vertical-align: text-bottom">' .
        '<path fill="#333" d="M9.75 24h-8.966c-0.211 0-0.412-0.089-0.554-0.244s-0.212-0.364-0.193-0.573c0.165-1.831 0.877-3.545 2.060-4.957 1.045-1.248 2.385-2.178 3.903-2.713l0-0.712c-0.842-0.577-1.555-1.389-2.077-2.372-0.603-1.136-0.922-2.451-0.922-3.803 0-1.876 0.607-3.647 1.709-4.985 1.137-1.38 2.661-2.14 4.291-2.14s3.155 0.76 4.291 2.14c1.102 1.338 1.709 3.108 1.709 4.985 0 1.351-0.319 2.666-0.922 3.802-0.522 0.983-1.235 1.796-2.077 2.372l-0 0.95c-0 0.414-0.336 0.75-0.75 0.75-0 0 0 0-0 0-0.414-0-0.75-0.336-0.75-0.75l0-1.364c0-0.268 0.143-0.516 0.375-0.65 1.594-0.92 2.625-2.926 2.625-5.112 0-3.102-2.019-5.625-4.5-5.625s-4.5 2.523-4.5 5.625c0 2.186 1.030 4.192 2.625 5.112 0.232 0.134 0.375 0.382 0.375 0.65l-0 1.674c0 0.336-0.223 0.63-0.546 0.722-1.451 0.41-2.733 1.243-3.707 2.406-0.807 0.964-1.351 2.096-1.597 3.312h8.1c0.414 0 0.75 0.336 0.75 0.75s-0.336 0.75-0.75 0.75z"></path>' .
				'<path fill="#198754" d="M15 24c-0.192 0-0.384-0.073-0.53-0.22l-3-3c-0.293-0.293-0.293-0.768 0-1.061s0.768-0.293 1.061 0l2.47 2.47 6.97-6.97c0.293-0.293 0.768-0.293 1.061 0s0.293 0.768 0 1.061l-7.5 7.5c-0.146 0.146-0.338 0.22-0.53 0.22z"></path>' .
	    '</svg>';
    }

    return $html;
  }

  protected function getTitle(): string
  {
    return 'n3t MultiCaptcha';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>'.$name.'</td>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
