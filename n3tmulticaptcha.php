<?php
/**
 * @package    n3t MultiCaptcha
 *
 * @author     Pavel Poles - n3t.cz
 * @copyright  © 2022 - 2023 Pavel Poles - n3t.cz. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @link       https://n3t.bitbucket.io
 */

\defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Http\HttpFactory;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Version;
use Joomla\CMS\Captcha\Captcha;
use Joomla\CMS\Form\Field\CaptchaField;
use Joomla\CMS\Factory;
use Joomla\CMS\Session\Session;
use Joomla\Event\DispatcherInterface;
use Joomla\Plugin\Captcha\n3tMultiCaptcha\Exception;
use Joomla\Registry\Registry;
use Joomla\Utilities\IpHelper;
use Joomla\CMS\Log\Log;
use Joomla\Plugin\Captcha\n3tMultiCaptcha\Country;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Application\ApplicationHelper;

class plgCaptchaN3tMultiCaptcha extends CMSPlugin
{
	/**
	 * Original Captcha field
	 *
	 * @var	   CaptchaField
	 * @since  4.0.0
	 */
	private $field = null;

	/**
	 * List of available Captcha plugins
	 *
	 * @var	   array
	 * @since  4.0.0
	 */
	private $captchaPlugins = [];

	/**
	 * Debug data
	 *
	 * @var	   array
	 * @since  4.0.0
	 */
	private $debug = [];

	/**
	 * Debug panel
	 *
	 * @var	   \Joomla\Plugin\Captcha\n3tMultiCaptcha\Debug\Panel
	 * @since  5.0.0
	 */
	private $debugPanel = null;

	/**
	 * Request IP address
	 *
	 * @var	   string
	 * @since  4.0.0
	 */
	private $ip = '127.0.0.1';

	/**
	 * Current user name
	 *
	 * @var	   ?string
	 * @since  4.0.0
	 */
	private $userName = null;

	/**
	 * Current user email
	 *
	 * @var	   ?string
	 * @since  4.0.0
	 */
	private $userEmail = null;

	private const LOG_FILE = 'n3t_multicaptcha.php';

	/**
	 * @param DispatcherInterface $subject
	 * @param array $config
	 */
	public function __construct(&$subject, array $config = [])
	{
		parent::__construct($subject, $config);

		$this->registerNamespace('Joomla\\Plugin\\Captcha\\n3tMultiCaptcha');

		$this->captchaPlugins = array_filter($this->paramList('captcha'), function($plugin) {
			return $plugin != 'n3tmulticaptcha';
		});

		$this->ip = $this->params->get('debug_ip', IpHelper::getIp());
		if (Version::MAJOR_VERSION < 4)
			$user = Factory::getUser();
		else
			$user = Factory::getApplication()->getIdentity();
		$this->userName = $this->params->get('debug_name', $user->name);
		$this->userEmail = $this->params->get('debug_email', $user->email);

		$this->debug['LOADED'] = true;
		$this->debug['DEBUG'] = $this->isDebug();
		$this->debug['IP'] = $this->ip;
		if ($this->isDebug())
			$this->debug['Real IP'] = IpHelper::getIp();
		$this->debug['User name'] = $this->userName;
		$this->debug['User email'] = $this->userEmail;
		$this->debug['FORMS'] = [];

		if (class_exists('\\n3tDebug') && method_exists('\\n3tDebug', 'registerPanel') && \n3tDebug::enabled()) {
			$this->debugPanel = new \Joomla\Plugin\Captcha\n3tMultiCaptcha\Debug\Panel($this);
			\n3tDebug::registerPanel($this->debugPanel);
		}

		if ($this->params->get('log', true)) {
			Log::addLogger(
				[
					'text_file'         => self::LOG_FILE,
					'text_entry_format' => "{DATETIME}\t{CLIENTIP}\t{MESSAGE}"
				],
				Log::ALL,
				['n3t_multicaptcha']
			);
		}
	}

	/**
	 * Checks, if current session is debug session (based on IP address)
	 *
	 * @return bool
	 *
	 * @since 4.0.5
	 */
	private function isDebug(): bool
	{
		return IpHelper::IPinList(IpHelper::getIp(), $this->getSubFormList('debug_ip_list', 'ip'));
	}

	/**
	 * Registers internal library namespace
	 *
	 * @param   string  $namespace
	 * @param   string  $path
	 *
	 * @since 4.0.0
	 */
	private function registerNamespace(string $namespace): void
	{
		$subpath = explode('\\', $namespace);
		$subpath =  array_pop($subpath);

		if (Version::MAJOR_VERSION < 4)
			JLoader::registerNamespace($namespace, __DIR__ . '/src/' . $subpath, false, false, 'psr4');
		else
			JLoader::registerNamespace($namespace, __DIR__ . '/src/' . $subpath);
	}

	/**
	 * Loads list from settings and normalize it
	 *
	 * @param   string  $paramName
	 *
	 * @since 4.0.0
	 */
	private function paramList(string $paramName, array $default = []): array
	{
		return array_filter((array) $this->params->get($paramName, $default), function($item) {
			return !!$item;
		});
	}

	/**
	 * Returns array of IP list from params
	 *
	 * @param   string  $paramName
	 * @param   string  $valueName
	 *
	 * @return array
	 *
	 * @since 4.0.0
	 */
	private function getSubFormList(string $paramName, string $valueName): array {
		$ipList = (array) $this->params->get($paramName);

		array_walk($ipList, function(&$item) use ($valueName) {
			$item = $item->{$valueName};
		});

		$ipList = array_filter($ipList, function($ip) {
			return !!$ip;
		});

		return $ipList;
	}

	/**
	 * Helper function to get JInput based on Joomla version
	 *
	 * @return \Joomla\Input\Input
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function getInput(): \Joomla\Input\Input
	{
		if (Version::MAJOR_VERSION < 4)
			return Factory::getApplication()->input;
		else
			return Factory::getApplication()->getInput();
	}

	/**
	 * Generates reversed IPv4
	 *
	 * @param   string  $ip
	 *
	 * @return string
	 *
	 * @since 4.0.0
	 */
	private function reverseIP(string $ip): string {
		return implode('.', array_reverse(explode('.', $ip)));
	}

	/**
	 * Helper function to throw exception and show message to user
	 *
	 * @param   string  $reason
	 * @param   string  $userError
	 * @param           ...$params
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function throwError(string $reason, ...$params): void
	{
		static $lang = null;
		if ($lang === null)
			$lang = Factory::getApplication()->getLanguage();

		$this->debug['ERROR'] = Text::sprintf('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_' . $reason, ...$params);
		Log::add(Text::sprintf('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_' . $reason, ...$params), Log::INFO, 'n3t_multicaptcha');

		if ($lang->hasKey('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_USER_' . $reason))
			throw new Exception(Text::sprintf('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_USER_' . $reason, ...$params));
		else
			throw new Exception(Text::_('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_USER_GENERAL'));
	}

	/**
	 * Checks current IP against DNS blacklist
	 *
	 * @param   string  $dnsbl
	 * @param   string  $prefix
	 * @param   string  $ignoreResponses
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkDNSBL(string $dnsbl, string $prefix = '', array $ignoreResponses = []): void
	{
		if (IpHelper::isIPv6($this->ip))
			return;

		$reverseIp = $this->reverseIP($this->ip);
		$url = $reverseIp . '.' . $dnsbl .'.';
		if ($prefix)
			$url = $prefix . '.' . $url;
		$this->debug[$dnsbl . ' request'] = $url;

		$response = gethostbyname($url);
		$this->debug[$dnsbl . ' response'] = $response;

		if ($response != $url && !in_array($response, $ignoreResponses))
			$this->throwError('BLACKLIST_API_IP', $this->ip, $dnsbl);
	}

	/**
	 * Checks continent and country against settings
	 *
	 * @param   string|null  $continent
	 * @param   string|null  $country
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkGeoIP(string $provider, ?string $continent, ?string $country): void
	{
		$continent = Country::validateContinent($continent);
		$country = Country::validate($country);

		$this->debug['GeoIP ' . $provider] = '[' . $continent . '] - [' . $country . ']';

		if (!$continent || !$country) {
			if ($this->params->get('allow_unknown_country', false))
				return;
			$this->throwError('GEOIP_UNKNOWN', $provider);
		}

		$continents = $this->paramList('continent_list');

		if ($this->params->get('continent_list_mode', 'deny') == 'deny' && in_array($continent, $continents))
			$this->throwError('GEOIP', $continent, $country, $provider);

		if ($this->params->get('continent_list_mode', 'deny') == 'allow' && !in_array($continent, $continents))
			$this->throwError('GEOIP', $continent, $country, $provider);

		$countries = $this->paramList('country_list');

		if ($this->params->get('country_list_mode', 'deny') == 'deny' && in_array($country, $countries))
			$this->throwError('GEOIP', $continent, $country, $provider);

		if ($this->params->get('country_list_mode', 'deny') == 'allow' && !in_array($country, $countries))
			$this->throwError('GEOIP', $continent, $country, $provider);
	}

	/**
	 * Randomly select one Captcha plugin from settings
	 *
	 * @return string|null
	 *
	 * @throws \Exception
	 * @since 4.0.0
	 */
	private function selectCaptchaPlugin(): ?string {
		static $plugin = null;

		if ($plugin === null && $this->captchaPlugins) {
			$plugin = $this->captchaPlugins[array_rand($this->captchaPlugins)];

			/** @var Session $session */
			$session = Factory::getApplication()->getSession();
			$session->set('n3t.multicaptcha.plugin', $plugin);
		}

		return $plugin;
	}

	/**
	 * Returns selected captcha plugin from session
	 *
	 * @return string|null
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function selectedCaptchaPlugin(): ?string {
		if (!$this->captchaPlugins)
			return null;

		static $plugin = null;
		if ($plugin === null) {
			$plugin = false;

			/** @var Session $session */
			$session = Factory::getApplication()->getSession();
			$storedPlugin  = $session->get('n3t.multicaptcha.plugin');
			if ($storedPlugin && array_search($storedPlugin, $this->captchaPlugins) !== false) {
				$plugin = $storedPlugin;
			}

			// Cache could disable correct session settings
			if (!$plugin && count($this->captchaPlugins) == 1) {
				$plugin = reset($this->captchaPlugins);
			}
		}

		if (!$plugin)
			$this->throwError('CAPTCHA_NO_PLUGIN');

		return $plugin;
	}

	/**
	 * returns true, if current IP is on whitelist, otherwise false
	 *
	 * @return bool
	 *
	 * @since 4.0.0
	 */
	private function isWhitelisted(): bool
	{
		if (IpHelper::IPinList($this->ip, $this->getSubFormList('ip_whitelist', 'ip')))
			return true;

		if ($this->params->get('whitelist_localhost', true) && IpHelper::IPinList($this->ip,
				[
					'127.0.0.1',
					'::1',
				]
			)
		)
			return true;

		if ($this->params->get('whitelist_private_networks', true) && IpHelper::IPinList($this->ip,
				[
					'10.0.0.0–10.255.255.255',
					'172.16.0.0–172.31.255.255',
					'192.168.0.0–192.168.255.255',
					'fd00:0000:0000:0000:0000:0000:0000:0000-fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff',
				]
			)
		)
			return true;

		return false;
	}

	/**
	 * Checks if current IP is not blacklisted
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkIpBlackList(): void
	{
		if (IpHelper::IPinList($this->ip, $this->getSubFormList('ip_blacklist', 'ip')))
			$this->throwError('IP_BLACKLIST');
	}

	/**
	 * Checks if session existed before
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkSession(): void
	{
		if (!$this->params->get('session', true))
			return;

		$session = Factory::getApplication()->getSession();
		if (!$session->get('n3t.multicaptcha.check'))
			$this->throwError('SESSION');
	}

	/**
	 * Checks cookie
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkCookie(): void
	{
		if (!$this->params->get('cookie', true))
			return;

		$input = $this->getInput();
		if (!$input->cookie->get(ApplicationHelper::getHash('n3t_multicaptcha')))
			$this->throwError('COOKIE');
	}

	/**
	 * Checks minimal filling time
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkMinTime(): void
	{
		if (!$this->params->get('min_time', true))
			return;

		$session = Factory::getApplication()->getSession();
		if (!$session->get('n3t.multicaptcha.time'))
			$this->throwError('MIN_TIME_SESSION');

		$now = (new Date())->toUnix();
		$this->debug['Form filling time'] = $now - $session->get('n3t.multicaptcha.time');
		if ($now - $session->get('n3t.multicaptcha.time') < $this->params->get('min_time_seconds', 3))
			$this->throwError('MIN_TIME', $this->params->get('min_time_seconds', 3));
	}

	/**
	 * Checks hidden field honeypot
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkHiddenField(array $data): void
	{
		if (!$this->params->get('hidden_field', true))
			return;

		$name = ApplicationHelper::getHash('n3t_email');
		if (!isset($data[$name]))
			return;

		if (!$data[$name])
			return;

		$this->throwError('HIDDEN_FIELD');
	}

	/**
	 * Checks javascript support
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkJSSupport(array $data): void
	{
		if (!$this->params->get('js_support', true))
			return;

		$name = ApplicationHelper::getHash('n3t_js');
		if (!isset($data[$name]))
			$this->throwError('JS_SUPPORT');

		if (!$data[$name])
			return;

		$this->throwError('JS_SUPPORT');
	}

	/**
	 * Checks data containing no links
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkLinks(array $data): void
	{
		if (!$this->params->get('links', false))
			return;

		$data = implode(' ', $data);
		$count = preg_match_all('~https?://~i', $data);
		$count += preg_match_all('~www\.~i', $data);
		if ($count > $this->params->get('links_count', 0)) {
			if ($this->params->get('links_count', 0))
				$this->throwError('LINKS_MAX', $this->params->get('links_count', 0));
			else
				$this->throwError('LINKS');
		}
	}

	/**
	 * Checks data containing no HTML
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkHtml(array $data): void
	{
		if (!$this->params->get('html', false))
			return;

		$data = implode(' ', $data);
		if ($data != strip_tags($data))
			$this->throwError('HTML');
	}

	/**
	 * Checks data containing no PHP opening tag
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkPHP(array $data): void
	{
		if (!$this->params->get('php', true))
			return;

		$data = implode(' ', $data);
		if (strpos($data, '<?') !== false)
			$this->throwError('PHP');
	}

	/**
	 * Checks data containing no script opening tag
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkJS(array $data): void
	{
		if (!$this->params->get('js', true))
			return;

		$data = implode(' ', $data);
		if (preg_match('~<script~i', $data))
			$this->throwError('JS');
	}

	/**
	 * Checks data not containing blacklisted words
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkWordsBlacklist(array $data): void
	{
		$blacklist = $this->getSubFormList('words_blacklist', 'word');
		if (!$blacklist)
			return;

		$data = implode(' ', $data);
		foreach ($blacklist as $word)
			if (strpos($data, $word) !== false)
				$this->throwError('WORDS_BLACKLIST', $word);
	}

	/**
	 * Checks data not matching blacklisted regular expressions
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkRegexBlacklist(array $data): void
	{
		$blacklist = $this->getSubFormList('regex_blacklist', 'regex');
		if (!$blacklist)
			return;

		$data = implode(' ', $data);
		foreach ($blacklist as $regex)
			if (preg_match($regex, $data))
				$this->throwError('REGEX_BLACKLIST', $regex);
	}

	/**
	 * Checks data containing only allowed characters
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkCharacters(array $data): void
	{
		if (!$this->params->get('character', false))
			return;

		$list = $this->paramList('character_list', ['Common', 'Latin']);
		if (empty($list))
			if ($this->params->get('character_list_mode', 'allow') == 'allow')
				$this->throwError('CHAR_BLACKLIST');

		$regExpr = '';
		foreach ($list as $characterSet)
			$regExpr.= '\\p{' . $characterSet . '}';

		$data = implode(' ', $data);
		if ($this->params->get('character_list_mode', 'allow') == 'deny') {
			$regExpr = '/[' . $regExpr . ']/u';
			if (preg_match($regExpr, $data))
				$this->throwError('CHAR_BLACKLIST');
		} else {
			$regExpr = '/^[' . $regExpr . ']*$/u';
			if (!preg_match($regExpr, $data))
				$this->throwError('CHAR_BLACKLIST');
		}
	}

	/**
	 * Checks original Captcha plugin response
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkCaptcha($code): void
	{
		// Virtuemart is calling onCheckAnswer for all published plugins, thus reCaptcha get validated twice and returns error
		$input = $this->getInput();
		if ($input->get('option') == 'com_virtuemart')
			return;

		$plugin = $this->selectedCaptchaPlugin();
		if ($plugin) {
			if ($this->field) {
				$captcha = Captcha::getInstance($plugin, [
					'namespace' => $this->field->namespace
				]);
			} else {
				$captcha = Captcha::getInstance($plugin, []);
			}

			$error = '';
			try {
				$result = $captcha->checkAnswer($code);
			} catch (\Exception $e) {
				$error = $e->getMessage();
			}

			if (!$result)
				$this->throwError('CAPTCHA_PLUGIN', $plugin, $error);
		}
	}

	/**
	 * Checks current IPv4 against StopForumSpam.com API
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkStopForumSpam(): void
	{
		if (!$this->params->get('stopforumspam', true))
			return;

		$url = 'http://api.stopforumspam.org/api?f=json&ip=' . $this->ip;
		if ($this->userEmail)
			$url.= '&emailhash=' . md5($this->userEmail);

		$http = HttpFactory::getHttp();
		$response = $http->get($url);
		if ($response->code == 200) {
			$response = json_decode($response->body);
			$this->debug['StopForumSpam.com response'] = $response;
			if ($response && $response->success && !isset($response->ip->error)) {
				if ($response->ip->appears && $response->ip->confidence > ($this->params->get('stopforumspam_confidence', 1) / 10))
					$this->throwError('BLACKLIST_API_IP', $this->ip, 'StopForumSpam.com');
				if (isset($response->ip->torexit) && $response->ip->torexit && $this->params->get('stopforumspam_tor_exits', true))
					$this->throwError('BLACKLIST_API_TOR', $this->ip, 'StopForumSpam.com');

				if ($this->userEmail) {
					if ($response->emailhash->appears && $response->emailhash->confidence > ($this->params->get('stopforumspam_confidence', 1) / 10))
						$this->throwError('BLACKLIST_API_EMAIL', $this->userEmail, 'StopForumSpam.com');
				}

				if ($this->params->get('geoip', false) && $this->params->get('geoip_stopforumspam', true) && $response->ip->country) {
					$country = Country::validate($response->ip->country);
					if ($country) {
						$continent = Country::continent($country);
						$this->checkGeoIp('StopForumSpam.com', $continent, $country);
					}
				}
			}
		} else {
			if (Version::MAJOR_VERSION < 4)
				$this->debug['StopForumSpam.com result'] = $response->code;
			else
				$this->debug['StopForumSpam.com result'] = $response->code . ' - ' . $response->getReasonPhrase();
		}
	}

	/**
	 * Checks current IPv4 against SpamHaus.org DNS Blackilist
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkSpamHaus(): void
	{
		if (!$this->params->get('spamhaus', false))
			return;

		$this->checkDNSBL('zen.spamhaus.org', '', [
			'127.255.255.10',
			'127.255.255.11',
			'127.255.255.252',
			'127.255.255.254',
			'127.255.255.255',
		]);
	}

	/**
	 * Checks current IPv4 against Sorbs.net DNS Blackilist
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkSorbs(): void
	{
		if (!$this->params->get('sorbs', false))
			return;

		$this->checkDNSBL('problems.dnsbl.sorbs.net');
		$this->checkDNSBL('l2.spews.dnsbl.sorbs.net');
	}

	/**
	 * Checks current IPv4 against SpamCop.net API
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkSpamCop(): void
	{
		if (!$this->params->get('spamcop', false))
			return;

		$this->checkDNSBL('bl.spamcop.net');
	}

	/**
	 * Checks current IPv4 against SpamCop.net DNS Blackilist
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkUceProtect(): void
	{
		if (!$this->params->get('uceprotect', false))
			return;

		$this->checkDNSBL('dnsbl-1.uceprotect.net');
		$this->checkDNSBL('dnsbl-2.uceprotect.net');
		$this->checkDNSBL('dnsbl-3.uceprotect.net');
	}

	/**
	 * Checks current IPv4 against DroneBl.org DNS Blackilist
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkDroneBl(): void
	{
		if (!$this->params->get('dronebl', false))
			return;

		$this->checkDNSBL('dnsbl.dronebl.org');
	}

	/**
	 * Checks current IPv4 against BotScout.com API
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkBotScout(): void
	{
		if (!$this->params->get('botscout', false))
			return;

		if (IpHelper::isIPv6($this->ip))
			return;

		$url = 'http://botscout.com/test/?multi&ip=' . $this->ip;
		if ($this->userEmail)
			$url.= '&mail=' . $this->userEmail;
		if ($this->params->get('botscout_api_key'))
			$url.= '&key=' . $this->params->get('botscout_api_key');

		$http = HttpFactory::getHttp();
		$response = $http->get($url);
		if ($response->code == 200) {
			$response = explode('|', $response->body);
			$this->debug['BotScout.com response'] = $response;
			if (count($response) == 8 && $response[0] == 'Y') {
				if (
					$response[2] == 'IP'
					&& $response[3] > $this->params->get('botscout_occurrences', 0)
				)
					$this->throwError('BLACKLIST_API_IP', $this->ip, 'BotScout.com');
				if (
					$this->userEmail
					&& $response[4] == 'MAIL'
					&& $response[5] > $this->params->get('botscout_occurrences', 0)
				)
					$this->throwError('BLACKLIST_API_EMAIL', $this->userEmail, 'BotScout.com');
			}
		} else {
			if (Version::MAJOR_VERSION < 4)
				$this->debug['BotScout.com result'] = $response->code;
			else
				$this->debug['BotScout.com result'] = $response->code . ' - ' . $response->getReasonPhrase();
		}
	}

	/**
	 * Checks current IPv4 against Project Honey Pot DNS Blackilist
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkProjectHoneyPot(): void
	{
		if (!$this->params->get('honeypot', false))
			return;

		if (!$this->params->get('honeypot_api_key'))
			return;

		$this->checkDNSBL('dnsbl.httpbl.org', $this->params->get('honeypot_api_key'));
	}

	/**
	 * Checks current IPv4 against Internal PHP GeoIP functions
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkGeoIpPHP(): void
	{
		if (!$this->params->get('geoip', false))
			return;

		if (!$this->params->get('geoip_php', false))
			return;

		if (!\function_exists('geoip_record_by_name')) {
			$this->debug['PHP GeoIP'] = 'Function geoip_record_by_name() does not exists';
			return;
		}

		if ($this->params->get('geoip_path')) {
			geoip_setup_custom_directory($this->params->get('geoip_path'));
		}

		if (!geoip_db_avail(GEOIP_COUNTRY_EDITION)) {
			$this->debug['PHP GeoIP'] = 'GeoIP country database not available';
			return;
		}

		$this->debug['PHP GeoIP Path'] = geoip_db_filename(GEOIP_COUNTRY_EDITION);

		$record = geoip_record_by_name($this->ip);
		if ($record) {
			$this->checkGeoIP('PHP Internal', $record['continent_code'], $record['country_code']);
		}
	}

	/**
	 * Checks current IPv4 against MaxMind GeoIP API
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkGeoIpMaxMind(): void
	{
		if (!$this->params->get('geoip', false))
			return;

		if (!$this->params->get('geoip_maxmind', false))
			return;

		if (!$this->params->get('geoip_maxmind_account_id')) {
			$this->debug['MaxMind GeoIP API'] = 'Missing Account/User ID';
			return;
		}

		if (!$this->params->get('geoip_maxmind_license_key')) {
			$this->debug['MaxMind GeoIP API'] = 'Missing license key';
			return;
		}

		$url = 'https://' . ($this->params->get('geoip_maxmind_mode', 'geolite2') == 'geolite2' ? 'geolite.info' : 'geoip.maxmind.com');
		$url.= '/geoip/v2.1/country/' . $this->ip;

		$options = new Registry([
			'userauth' => $this->params->get('geoip_maxmind_account_id'),
			'passwordauth' => $this->params->get('geoip_maxmind_license_key'),
		]);
		$http = HttpFactory::getHttp($options);
		$response = $http->get($url);

		if ($response->code == 200) {
			$response = json_decode($response->body);
			$this->debug['MaxMind GeoIP response'] = $response;
			if ($response)
				$this->checkGeoIP('MaxMind.com', $response->continent->code, $response->country->iso_code);
		} else {
			if (Version::MAJOR_VERSION < 4)
				$this->debug['MaxMind GeoIP result'] = $response->code;
			else
				$this->debug['MaxMind GeoIP result'] = $response->code . ' - ' . $response->getReasonPhrase();
			$response = json_decode($response->body);
			$this->debug['MaxMind GeoIP response'] = $response;
		}
	}

	/**
	 * Checks data against Akismet API
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkAkismet(array $data): void
  {
	  if (!$this->params->get('akismet', false))
			return;

	  if (!$this->params->get('akismet_api_key')) {
		  $this->debug['Akismet API'] = 'Missing API key';
		  return;
	  }

	  $url = 'https://' . $this->params->get('akismet_api_key') . '.rest.akismet.com/1.1/comment-check';
	  $postData = [
			'blog' => Uri::base(),
			'user_ip' => $this->ip,
			'user_agent' => $this->getInput()->server->get('HTTP_USER_AGENT', '', 'raw'),
			'referrer' => $this->getInput()->server->get('HTTP_REFERER', '', 'raw'),
			'comment_content' => implode(' ', $data),
		  'blog_charset' => 'UTF-8',
	  ];

		if ($this->userEmail)
			$postData['comment_author_email'] = $this->userEmail;
	  if ($this->userName)
		  $postData['comment_author'] = $this->userName;


		$this->debug['Akismet data'] = $postData;

	  $http = HttpFactory::getHttp();
	  $response = $http->post($url, $postData);

	  if ($response->code == 200) {
		  $this->debug['Akismet response'] = $response->body;
		  if ($response->body == 'true')
			  $this->throwError('AKISMET');
	  } else {
		  if (Version::MAJOR_VERSION < 4)
			  $this->debug['Akismet result'] = $response->code;
		  else
			  $this->debug['Akismet result'] = $response->code . ' - ' . $response->getReasonPhrase();
		  $this->debug['Akismet response'] = $response->body;
	  }
  }

	/**
	 * Checks detected languages against language filter
	 *
	 * @param   string  $provider
	 * @param   array  $detectedLanguages
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkLanguage(string $provider, array $detectedLanguages): void
	{
		if (!$detectedLanguages)
			return;

		$langList = $this->paramList('lang_list');
		if (!$langList)
			return;

		$result = array_intersect($detectedLanguages, $langList);

		if ($result && $this->params->get('lang_list_mode', 'deny') == 'deny')
			$this->throwError('LANGUAGE', $provider);

		if (!$result && $this->params->get('lang_list_mode', 'deny') == 'allow')
			$this->throwError('LANGUAGE', $provider);
	}

	/**
	 * Checks data against language filter using internal functions
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkLanguagePHP(array $data): void
	{
		if (!$this->params->get('lang', false))
			return;

		$data = implode(' ', $data);
		if (strlen($data) < $this->params->get('lang_min_length', 200)) {
			$this->debug['PHP language detection'] = 'Skipping, text too short';
			return;
		}

		$this->registerNamespace('LanguageDetection');

		$t = new \LanguageDetection\Trainer();
		$t->setMaxNgrams((int)$this->params->get('lang_ngrams', 310));
		$t->learn();

		$ld = new \LanguageDetection\Language([], __DIR__ . '/src/LanguageDetection/resources/');
		$ld->setMaxNgrams((int)$this->params->get('lang_ngrams', 310));
		$detection = $ld->detect($data)->close();
		$this->debug['PHP language detection'] = $detection;

		$minScore = (int)$this->params->get('lang_min_score', 60) / 100;
		$detection = array_filter($detection, function($score) use ($minScore) {
			return $score >= $minScore;
		});
		$detection = array_keys($detection);

		$this->debug['PHP languages detected'] = $detection;
		$this->checkLanguage('Internal', $detection);
	}

	/**
	 * Checks data against language filter using DetectLanguage.com API
	 *
	 * @param   array  $data
	 *
	 * @throws Exception
	 * @since 4.0.0
	 */
	private function checkLanguageAPIDetectLanguage(array $data): void
	{
		if (!$this->params->get('detectlanguage', false))
			return;

		if (!$this->params->get('detectlanguage_api_key')) {
			$this->debug['DetectLanguage.com API'] = 'MIssing API key';
			return;
		}

		$data = implode(' ', $data);
		if (strlen($data) < $this->params->get('lang_min_length', 200)) {
			$this->debug['DetectLanguage.com'] = 'Skipping, text too short';
			return;
		}

		$this->registerNamespace('DetectLanguage');
		\DetectLanguage\DetectLanguage::setApiKey($this->params->get('detectlanguage_api_key'));

		$detection = \DetectLanguage\DetectLanguage::detect($data);
		$this->debug['DetectLanguage.com response'] = $detection;

		$minConfidence = (int)$this->params->get('detectlanguage_min_confidence', 15);
		$detection = array_filter($detection, function($item) use ($minConfidence) {
			return $item->confidence >= $minConfidence;
		});

		array_walk($detection, function(&$item) {
			$item = $item->language;
		});

		$this->debug['DetectLanguage.com detected'] = $detection;
		$this->checkLanguage('DetectLanguage.com', $detection);
	}

	/**
	 * Joomla! event
	 *
	 * @param   CaptchaField      $field
	 * @param   SimpleXMLElement  $element
	 *
	 * @since 4.0.0
	 */
	public function onSetupField(CaptchaField $field, \SimpleXMLElement $element): void
	{
		$this->field = $field;

		$element['hiddenLabel'] = 'true';
	}

	/**
	 * Joomla! event
	 *
	 * @param   string|null  $name
	 * @param   string|null  $id
	 * @param   string|null  $class
	 *
	 * @return string
	 *
	 * @since 4.0.0
	 */
	public function onDisplay(?string $name = null, ?string $id = null, ?string $class = null): string
  {
		if ($this->isWhitelisted()) {
			$this->debug['WHITELISTED'] = true;
			return '';
		}

	  $this->debug['FORMS'][] = [
			'name' => $name,
			'id' => $id,
			'class' => $class,
			'field' => $this->field,
	  ];
	  $this->selectCaptchaPlugin();

	  $html = '';
		$plugin = $this->selectCaptchaPlugin();
	  if ($plugin) {
			if ($this->field) {
				$captcha = Captcha::getInstance($plugin, [
					'namespace' => $this->field->namespace
				]);
			} else {
				$captcha = Captcha::getInstance($plugin, []);
			}

		  $html .= $captcha->display($name ?: '', $id ?: '', $class ?: '');
	  }

		if ($this->params->get('session', true)) {
			$session = Factory::getApplication()->getSession();
			$session->set('n3t.multicaptcha.check', true);
		}

	  if ($this->params->get('cookie', true)) {
			$input = $this->getInput();
			if (!$input->cookie->get(ApplicationHelper::getHash('n3t_multicaptcha')))
				$input->cookie->set(ApplicationHelper::getHash('n3t_multicaptcha'), 1);
	  }

	  if ($this->params->get('min_time', true)) {
		  $session = Factory::getApplication()->getSession();
		  $session->set('n3t.multicaptcha.time', (new Date())->toUnix());
	  }

	  static $index = 0;
		$index++;
	  if ($this->params->get('hidden_field', true)) {
		  $id = 'n3tMultiCaptcha' . $index;
		  if ($this->field)
				$html.= '<span><input type="email" name="' . $this->field->formControl . '[' . ApplicationHelper::getHash('n3t_email') . ']" id="' . $id . '" tabindex="-1" autocomplete="email-no"/></span>';
			else
				$html.= '<span><input type="email" name="' . ApplicationHelper::getHash('n3t_email') . '" id="' . $id . '" tabindex="-1" autocomplete="email-no"/></span>';
		  $html.= '<script type="text/javascript">' .
				'document.addEventListener("DOMContentLoaded", function() {' .
        'document.getElementById("' . $id .'").parentNode.className = "n3tMultiCaptchaField' . $index . '";' .
        '});'.
		    '</script>';
		  $html .= '<style type="text/css">' .
			  '.n3tMultiCaptchaField' . $index . ' {display: none}' .
		    '</style>';
	  }

	  if ($this->params->get('js_support', true)) {
		  $id = 'n3tMultiCaptchaJS' . $index;
		  if ($this->field)
		    $html.= '<input type="hidden" name="' . $this->field->formControl . '[' . ApplicationHelper::getHash('n3t_js') . ']" id="' . $id . '" value="1" />';
			else
				$html.= '<input type="hidden" name="' . ApplicationHelper::getHash('n3t_js') . '" id="' . $id . '" value="1" />';
		    $html.= '<script type="text/javascript">' .
			    'document.addEventListener("DOMContentLoaded", function() {' .
			    'document.getElementById("' . $id .'").value = "0";' .
			    '});' .
		      '</script>';
	  }

	  return $html;
  }

	/**
	 * Joomla! event
	 *
	 * @param   string|null  $code
	 *
	 * @return bool
	 *
	 * @since 4.0.0
	 */
	public function onCheckAnswer($code = null): bool
  {
	  if ($this->isWhitelisted()) {
		  $this->debug['WHITELISTED'] = true;
		  return true;
	  }

		$this->loadLanguage();
		if (Version::MAJOR_VERSION < 4) {
			$trace = debug_backtrace();
			$trace = array_filter($trace, function($item) {
				return isset($item['class']) && $item['class'] == 'Joomla\\CMS\\Form\\Rule\\CaptchaRule';
			});

			if ($trace) {
				$trace = array_shift($trace);

				/** @var \SimpleXMLElement $element */
				$element = $trace['args'][0];
				/** @var \Joomla\CMS\Form\Form $form */
				$form = $trace['args'][4];

				if ($form && $element)
					$this->field = $form->getField($element['name']);
			}
		}

	  $input = $this->getInput();
		if ($this->field)
	    $data  = $input->request->get($this->field->formControl, [], 'raw');
		else
			$data  = $input->request->getArray();

	  $this->debug['Form data'] = $data;

		$whitelist = $this->getSubFormList('fields_whitelist', 'name');
		$data = array_filter($data, function ($key) use ($whitelist) {
			return !in_array($key, $whitelist);
		}, ARRAY_FILTER_USE_KEY);
	  $this->debug['Form data filtered'] = $data;

	  try
	  {
		  // Basic checks
		  $this->checkIpBlacklist();
		  $this->checkSession();
		  $this->checkCookie();
		  $this->checkMinTime();
		  $this->checkHiddenField($data);
		  $this->checkJSSupport($data);

		  // Content checks
		  $this->checkLinks($data);
		  $this->checkPHP($data);
		  $this->checkJS($data);
		  $this->checkHtml($data);
		  $this->checkWordsBlacklist($data);
		  $this->checkRegExBlacklist($data);
		  $this->checkCharacters($data);

		  // Captcha plugin
		  $this->checkCaptcha($code);

		  // Blacklists
		  $this->checkStopForumSpam();
		  $this->checkSpamHaus();
		  $this->checkSorbs();
		  $this->checkSpamCop();
		  $this->checkUceProtect();
		  $this->checkDroneBl();
		  $this->checkBotScout();
		  $this->checkProjectHoneyPot();

		  // Geo IP
		  $this->checkGeoIpPHP();
		  $this->checkGeoIpMaxMind();

		  // Content API
		  $this->checkAkismet($data);

		  // Language
		  $this->checkLanguagePHP($data);
		  $this->checkLanguageAPIDetectLanguage($data);
	  } catch (Exception $e) {
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'error');
		  $this->debug['EXCEPTION'] = $e;
		  return false;
	  } catch (\Exception $e) {
			$this->loadLanguage();
		  $this->debug['EXCEPTION'] = $e;
		  Log::add(Text::sprintf('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_EXCEPTION', get_class($e), $e->getMessage()), Log::WARNING, 'n3t_multicaptcha');
		  Factory::getApplication()->enqueueMessage('PLG_CAPTCHA_N3TMULTICAPTCHA_ERROR_USER_GENERAL', 'error');
		  return false;
	  }

	  $this->debug['RESULT'] = 'OK';

		return true;
  }

	/**
	 * n3tDebug collectDebugData event
	 *
	 * @since 4.0.0
	 */
	public function collectDebugData()
	{
		return $this->debug;
	}

	/**
	 * __clone magic method - copy correct instance of plugin to debug panel
	 *
	 * @since 5.0.0
	 */
	public function __clone()
	{
		if ($this->debugPanel) {
			$this->debugPanel->setPlugin($this);
		}
	}
}
