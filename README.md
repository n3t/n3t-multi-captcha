n3t Multi Captcha
=================

[![Version](https://img.shields.io/badge/Latest%20version-5.0.0-informational)][N3TMULTICAPTCHA]
[![Release date](https://img.shields.io/badge/Release%20date-2024--06--13-informational)][N3TMULTICAPTCHA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.4-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-multi-captcha/badge/?version=latest)](https://n3t-multi-captcha.readthedocs.io/en/latest/?badge=latest)

n3t Multi Captcha allows to randomly switch between selected Captcha plugins.
It also add additional security level by checking users IP address against black lists.

Install instructions
----------------------------
 * install plugin using Joomla! installer
 * enable plugin
 * [read the documentation][DOCS]

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[DOCS]: https://n3t-multi-captcha.readthedocs.io/
[N3TMULTICAPTCHA]: https://n3t.bitbucket.io/extension/n3t-multi-captcha
